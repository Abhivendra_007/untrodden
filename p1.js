var prArr = [];
function makePromise(i , timeDuration){
	return new Promise(function(resolve , reject){
        resolve(i );
	});
}

for (var i=1; i<=100; i++) {
	prArr.push(makePromise(i));
}

function resolveSequence(count){
	if(!count){count=0};
	if(count<100){
		prArr[count].then(function(here , timeDuration){
			setTimeout(function(i) {
				console.log(i);
        		resolveSequence(count+1);
    		}.bind(this , here) , Math.random()*1000);
		} , function(err){
			console.log(err);
		})
	}
}
	
resolveSequence();